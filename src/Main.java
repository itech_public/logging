/**
 * Created by heiko on 12.12.16.
 */
import org.pmw.tinylog.Logger;

public class Main
{
    private static int beispielRechner(int i) {
        int x = 0;
        while (i <= 1) {
            x = x + i;
            Logger.info("Meine Info-Meldung. " +
                    "Der Wert von j beträgt {}, der Wert von i Beträgt {}", i, x);
            Logger.warn("Meine Warn-Meldung");
            Logger.error("Meine Error-Meldung");
            Logger.debug("Meine Debug-Meldung");
            Logger.trace("Meine Trace-Meldung");
            // Weitere Erläuterungen http://www.tinylog.org/de
            i++;
        }
        return x;
    }
    public static int beispielStatic (int i) {
        return beispielRechner(i);
    }
    public int beispielOhneStatic (int i) {
        return beispielRechner(i);
    }
    public static void main( String[] args ) {
        beispielStatic(1);
        //Main m = new Main ();
        //m.beispielOhneStatic(1);
    }
}